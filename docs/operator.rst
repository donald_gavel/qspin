########
operator
########

Quantum operators, or 'observables'

.. py:currentmodule:: qspin

Class
=====

.. autoclass:: operator
    :members:
    :exclude-members: ops, matrix, eig
    
    **attributes**

    .. py:attribute:: matrix
    
        the matrix representation of the operator: `A.matrix` = :math:`\left[\Braket{ e_i | A | e_j }\right]`, where :math:`\Ket{e_i}` are the basis states

    .. py:attribute:: observables
    
        a list of the operator observable values (the eigenvalues, :math:`\lambda_k` )
        
    .. py:attribute:: eigenstates
    
        a list of the operator eigenstates :math:`\Ket{\Phi_k}`, as state vectors [:math:`\Braket{e_i|\Phi_k}`]
        
    .. py:attribute:: basis
    
        a list of the basis states :math:`\Ket{e_i}`
        

    **methods**
    
Predefined operators
====================

.. autoattribute:: qspin.operator.ops

