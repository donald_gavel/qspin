.. qspin documentation master file, created by
   sphinx-quickstart on Mon Apr 30 20:18:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Quantum Spin
============

This is a little package that will help with learning how quantum spin and entanglement work.
It is meant to complement some of the "theoretical minimum" lectures and other web resources:

[
`Quantum state <https://en.wikipedia.org/wiki/Quantum_state>`_; 
`Pauli matrices <https://en.wikipedia.org/wiki/Pauli_matrices>`_;
`Singlet state <https://en.wikipedia.org/wiki/Singlet_state>`_;
`Triplet state <https://en.wikipedia.org/wiki/Triplet_state>`_;
`Density matrix <https://en.wikipedia.org/wiki/Density_matrix>`_;
`Quantum entanglement <https://en.wikipedia.org/wiki/Quantum_entanglement>`_;
`Entropy <https://en.wikipedia.org/wiki/Von_Neumann_entropy>`_;
`Quantum logic gate <https://en.wikipedia.org/wiki/Quantum_logic_gate>`_
]

- Book: **Quantum Mechanics - The Theoretical Minimum**, Leanoard Susskind and Art Friedman, Basic Books, 2014. (mostly chapters 6&7)
- http://theoreticalminimum.com/courses/quantum-mechanics/2012/winter/lecture-6 and lecture-7

`Link to documentation on readthedocs <http://qspin.readthedocs.io>`_

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   Quantum Spin <userdoc>
   API <qspin>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
