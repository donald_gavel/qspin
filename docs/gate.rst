####
gate
####

.. py:currentmodule:: qspin

Quantum gates. Useful for defining quantum computing operations.

:class:`gate` inherits from :class:`operator`

.. py:currentmodule:: qspin

Class
=====

.. autoclass:: qspin.gate
    :members:
    :exclude-members: gates, url, copy

Predefined gates
================
.. autoattribute:: qspin.gate.gates

Reference
=========

https://en.wikipedia.org/wiki/Quantum_logic_gate
