#####
state
#####
    
State vectors

Functions
=========

.. autofunction:: qspin.ket

.. autofunction:: qspin.bra

.. autofunction:: qspin.set_base_repr

Predefined states
=================

Certain state are pre-defined:
up: `u`, down: `d`, singlet state: `s`,
triplet states: `t1 = u**u`, `t2 = (u**d-d**u).N`, and `t3 = d**d`

.. math::

    s &= \left( \Ket{ud} - \Ket{du} \right) / \sqrt{2} \\
    t_1 &= \Ket{uu} \\
    t_2 &= \left( \Ket{ud} + \Ket{du} \right) / \sqrt{2} \\
    t_3 &= \Ket{dd}

Class
=====

.. autoclass:: qspin.state
    :members:
    :exclude-members: density1, normalize, normalized

    **attributes**

    .. py:attribute:: phi
    
        the vector (wave function) representation of the state
    
    .. py:attribute:: n_particles
    
        the number of particles in the state. This is :math:`n_{particles} = {\rm log}_2n` where :math:`n` is the length of the state vector

    .. py:attribute:: basis
    
        a list of the basis states :math:`\Ket{e_i}`, :math:`i=1,2,\dots,n` where :math:`n` is the length of the state vector
        
    **methods**
    
Density matrix functions
========================

.. autofunction:: qspin.density

.. autofunction:: qspin.ptrace

.. autofunction:: qspin.entropy

References
==========

| https://en.wikipedia.org/wiki/Quantum_state
| https://en.wikipedia.org/wiki/Density_matrix
|

