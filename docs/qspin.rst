qspin API reference
===================

.. automodule:: qspin

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   state
   operator
   gate

