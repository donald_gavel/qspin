# -*- coding: utf-8 -*-
"""
Create pauli spin matrices and other quantum mechanical operators
for collections of single-spin particles.
Manipulate quantum states and simulate quantum computer circuits.
"""
from __future__ import print_function
import sys
import copy
import itertools
from types import SimpleNamespace
from numbers import Number
import numpy as np
from numpy import pi,exp,sin,cos
from numpy import diag,matrix
from numpy.linalg import matrix_power
from scipy.linalg import sqrtm,block_diag,expm,logm,fractional_matrix_power
from fractions import Fraction

__version__ = '2.3.4'
if sys.version_info.major == 3:
    unicode = str

debug = False

sign = lambda a: '+' if a>0 else '-' if a<0 else '+'

stripone = lambda x: x.rstrip('1') if x[1:]=='1' else x
iszero = lambda x: x in ['0','+0','-0']
flatten = lambda x: [e for sl in x for e in sl]

#============= Print Formatting Options =============
usqrt = '\u221a'
upi = '\u03c0'
uphi = '\u03d5' # alt: '\u03c6'
psq = [x**2 for x in range(1,11)]
cmax,xmax = 25,25
srl = flatten([[(x,c,c*np.sqrt(x)) for x in range(2,xmax+1) if x not in psq] for c in range(1,cmax+1)])

default_printoptions = {
    'format':'+0.3f',
    'prec':3,
    'sup':True,
    'nz':False,
    'sqrtpi':True,
    'dFmax':1000000,
}

printoptions = SimpleNamespace(**default_printoptions)

def set_printoptions(*args,**kwargs):
    global printoptions
    if args:
        assert args[0] == 'default'
        printoptions = SimpleNamespace(**default_printoptions)
        return            
    if not kwargs:
        return printoptions
    if 'format' in kwargs: raise Exception('cannot set format string; it is derived from sup and prec')
    for key,value in kwargs.items():
        setattr(printoptions,key,value)
    if 'sup' in kwargs or 'prec' in kwargs:
        forg = 'f' if printoptions.sup else 'g'
        prec = printoptions.prec
        printoptions.format = f'+0.{prec}{forg}'

def frepr(x):
    ''' string representation of float
    '''
    #fmt = '%%+0.%d%s'%(_prec,{True:'f',False:'g'}[_sup])
    fmt = f'%{printoptions.format}'
    s = (fmt%x)
    #if _sup: s = s.rstrip('0').rstrip('.')
    if printoptions.sup: s = s.rstrip('0').rstrip('.')
    return s

def crepr(z,plus_sign=False):
    ''' string representation of complex
    '''
    x,y = z.real,z.imag
    if printoptions.sqrtpi:
        sx,sy = [f'+{prepr(q)}' for q in [x,y]]
        sx,sy = [x.replace('+-','-',1) for x in [sx,sy]]
    else:
        sx,sy = [frepr(q) for q in [x,y]]
    if iszero(sy): # real
        #if printoptions.sqrtpi and not plus_sign: sx = sx.lstrip('+')
        if not plus_sign: sx = sx.lstrip('+')
        s = sx
    elif iszero(sx): # imaginary
        if not printoptions.sqrtpi:
            sy = stripone(sy)
            s = f'{sy}i'
        if printoptions.sqrtpi:
            sy = sy.lstrip('+')
            if sy.startswith('-'):
                sgn = '-'
                sy = sy[1:]
            else:
                sgn = '' if not plus_sign else '+'
            if sy == '1':
                sy = 'i'
            elif sy.startswith('1/'):
                sy = 'i/'+sy[2:]
            else:
                sy = f'{sy}i'
            s = f'{sgn}{sy}'
    else: # complex
        if not printoptions.sqrtpi:
            sx = sx.lstrip('+')
            sy = stripone(sy)
            s = f'({sx}{sy}i)'
            #s = '+(%s%si)'%(sx.lstrip('+'),stripone(sy))
        if printoptions.sqrtpi:
            if not plus_sign: sx = sx.lstrip('+')
            sy = sy.lstrip('+')
            if sy.startswith('-'):
                sgn = '-'
                sy = sy[1:]
            else:
                sgn = '+'
            if sy == '1':
                sy = 'i'
            elif sy.startswith('1/'):
                sy = 'i/'+sy[2:]
            else:
                sy = f'{sy}i'
            
            s = f'{sx}{sgn}{sy}'
            
    return s

def mprepr(z,onaxis=False,mlen=6):
    '''string representation of complex in magnitude-phase form
    '''
    if not isinstance(z,qcomplex): z = qcomplex(z)
    m,p = z.abs,z.angle
    if np.isclose(m,0): return f'0'
    sgn = sign(p).lstrip('+')
    sm = srrepr(m)

    #     -- magnitude --
    if np.isclose(p,0): # positive real axis
        s = f'{sm}'
        return s
    else:
        if sm == '1': sm = ''
        if '/' in sm: sm = f'({sm})'
    
    #        -- phase --
    p = z.angle
    sp = prepr(p.abs)
    sgn = sign(p).lstrip('+')
    if sp == '0':
        if sm == '': sm = '1'
        s = f'{sm}'
        return s
    if sp == '1': sp = ''
    if sp.startswith('1/'): sp = sp[1:]

    s = f'{sm}e^{sgn}i{sp}'

    # recognize if number is on real or imaginary axis
    if onaxis and 'e^' in s:
        pa = p.abs
        if np.isclose(pa,pi/2): # imaginary axis
            if '/' in sm:
                sm = sm.strip('()')
                if sm[0:2] == '1/':
                    sm = sm.replace('1','i',1)
                else:
                    sm = sm.replace('/','i/')
                s = f'{sgn}{sm}'
            else:
                s = f'{sgn}{sm}i'
        elif np.isclose(pa,pi): # negative real axis
            if '/' in sm:
                sm = sm.strip('()')
            if np.isclose(m,1):
                sm = '1'
            s = f'-{sm}'
    return s

def prepr(a,mlen=6):
    '''representation of phase angle
    '''
    if isinstance(a,complex):
        assert np.isclose(a.imag,0)
        a = qfloat(a.real)
    if not isinstance(a,qfloat): a = qfloat(a)
    if np.isclose(a,0): return'0'
    sgn = sign(a).lstrip('+')
    p = (a/pi).abs
    sp = srrepr(p)
    if '.' not in sp and sp != '0': # reasonable factor of pi
        if '/' in sp:
            spn,spd = sp.split('/')
            if spn == '1': spn = ''
            sp = f'{sgn}{spn}{upi}/{spd}'
        else:
            if sp == '1': sp = ''
            sp = f'{sgn}{sp}{upi}'
    else: # not a good factor of pi, use the best representation of radians phase
        sp = srrepr(a.abs)
        sp = f'{sgn}{sp}'
    return sp

def srrepr(x):
    '''check if value x is in a list of known square roots of integers
    and return a string rendition with the unicode square root symbol
    '''
    if isinstance(x,int) or x.is_integer(): return frepr(x).lstrip('+')
    f = Fraction(x**2).limit_denominator(printoptions.dFmax)
    num = np.sqrt(f.numerator)
    den = np.sqrt(f.denominator)
    sgn = sign(x).lstrip('+')
    
    bail = False
    if num.is_integer():
        num_str = frepr(num).lstrip('+')
    else:
        fnum = [(j,c) for j,c,y in srl if np.isclose(num,y)]
        if fnum:
            ind = np.argmax([c for j,c in fnum])
            j,c = fnum[ind]
            if c == 1: c = ''
            num_str = f'{c}{usqrt}{j}'
        else:
            num_str = frepr(num).lstrip('+')
            bail = True

    if den.is_integer():
        den_str = frepr(den).lstrip('+')
    else:
        fden = [(j,c) for j,c,y in srl if np.isclose(den,y)]
        if fden:
            ind = np.argmax([c for j,c in fden])
            j,c = fden[ind]
            if c == 1: c = ''
            den_str = f'{c}{usqrt}{j}'
        else:
            den_str = frepr(den).lstrip('+')
            bail = True
    
    if bail: return frepr(x).lstrip('+')
    
    if den_str == '1':
        den_str = div_str = ''
    else:
        div_str = '/'
    return f'{sgn}{num_str}{div_str}{den_str}'

#============= Real and Complex Numbers ==========
class qfloat(float):
    def __new__(self, value):
        return float.__new__(self, value)
    def __init__(self, value):
        float.__init__(value)
    def __repr__(self):
        if printoptions.sqrtpi:
            s = prepr(self)
        else:
            s = frepr(self)
        return s
    def srrepr(self):
        return srrepr(self)
    def __pow__(self,other):
        if isinstance(other,complex):
            return qcomplex(float(self)**other)
        return qfloat(float(self)**other)
    def __xor__(self,other):
        return self.__pow__(other)
    def __add__(self,other):
        if isinstance(other,complex):
            return qcomplex(float(self)+complex(other))
        else:
            return qfloat(float(self)+float(other))
    def __radd__(self,other):
        return self.__add__(other)
    def __sub__(self,other):
        if isinstance(other,complex):
            return qcomplex(float(self)-complex(other))
        else:
            return qfloat(float(self)-float(other))
    def __rsub__(self,other):
        return -self.__sub__(other)
    def __mul__(self,other):
        if isinstance(other,complex):
            return qcomplex(float(self)*complex(other))
        elif isinstance(other,(operator,state,np.ndarray,np.matrix)):
            return float(self)*other
        elif np.isscalar(other):
            return qfloat(float(self)*float(other))
        else:
            raise Exception(f'type {type(other)} not supported for qfloat multiplication')
    def __rmul__(self,other):
        return self.__mul__(other)
    def __truediv__(self,other):
        if isinstance(other,complex):
            return qcomplex(float(self)/complex(other))
        else:
            return qfloat(float(self)/float(other))        
    def __rtruediv__(self,other):
        if isinstance(other,complex):
            return qcomplex(complex(other)/complex(self))
        else:
            return qfloat(float(other)/float(self))
    def __neg__(self):
        return qfloat(-float(self))
    def exp(self):
        return qfloat(np.exp(float(self)))
    def log(self):
        return qfloat(np.log(float(self)))
    def sqrt(self):
        if self>0: return qfloat(np.sqrt(float(self)))
        return qcomplex(np.sqrt(complex(self)))
    @property
    def real(self):
        return self
    @property
    def imag(self):
        return qfloat(0)
    def conjugate(self):
        return self
    @property
    def angle(self):
        return qfloat(np.angle(complex(self)))
    @property
    def abs(self):
        return qfloat(np.abs(float(self)))
    @property
    def abs2(self):
        return qfloat(np.abs(float(self))**2)

class qcomplex(complex):
    def __new__(self, value):
        return complex.__new__(self, value)
    def __init__(self, value):
        complex.__init__(value)
    def __repr__(self):
        s = crepr(self)
        return s
    def mprepr(self,**kwargs):
        return mprepr(self,**kwargs)
    def __pow__(self,other):
        return qcomplex(complex(self)**other)
    def __xor__(self,other):
        return self.__pow__(other)
    def __add__(self,other):
        return qcomplex(complex(self)+complex(other))
    def __radd__(self,other):
        return self.__add__(other)
    def __sub__(self,other):
        return qcomplex(complex(self)-complex(other))
    def __rsub__(self,other):
        return qcomplex(complex(other)-complex(self))
    def __mul__(self,other):
        if isinstance(other,(operator,state,np.ndarray,np.matrix)):
            return complex(self)*other
        elif np.isscalar(other):
            return qcomplex(complex(self)*complex(other))
        else:
            raise Exception(f'type {type(other)} not supported for qcomplex multiplication')
    def __rmul__(self,other):
        return self.__mul__(other)
    def __neg__(self):
        return qcomplex(-complex(self))
    def __truediv__(self,other):
        return qcomplex(complex(self)/complex(other))
    def __rtruediv__(self,other):
        return qcomplex(complex(other)/complex(self))
    def exp(self):
        return qcomplex(np.exp(complex(self)))
    def log(self):
        return qcomplex(np.log(complex(self)))
    def sqrt(self):
        return qcomplex(np.sqrt(complex(self)))
    @property
    def real(self):
        return qfloat(np.real(complex(self)))
    @property
    def imag(self):
        return qfloat(np.imag(complex(self)))
    @property
    def angle(self):
        return qfloat(np.angle(complex(self)))
    @property
    def abs(self):
        return qfloat(np.abs(complex(self)))
    @property
    def abs2(self):
        return qfloat(np.abs(complex(self))**2)
    def conjugate(self):
        return qcomplex(np.conjugate(complex(self)))

exp = lambda x: qcomplex(x).exp() if np.isscalar(x) else np.exp(x)
def sqrt(x):
    if np.isscalar(x):
        if isinstance(x,complex): x=qcomplex(x)
        else: x = qfloat(x)
        return x.sqrt()
    else:
        return np.sqrt(x)
#sqrt = lambda x: x.sqrt() if isinstance(x,(qfloat,qcomplex)) else np.sqrt(x)
abs = lambda x: qcomplex(x).abs if np.isscalar(x) else np.abs(x)
abs2 = lambda x: qcomplex(x).abs**2 if np.isscalar(x) else np.abs(x)**2
angle = lambda x: qcomplex(x).angle if np.isscalar(x) else np.angle(x)
i = qcomplex(1j)
pi = qfloat(np.pi)

#============== Quantum States and Densities ================
base_reprs = {
    'ud':{'up':'u','down':'d'},
    'lr':{'up':'l','down':'r'},
    '01':{'up':'0','down':'1'},
    'arrow':{'up':u'\u2191','down':u'\u2193'},
    'lrarrow':{'up':u'\u2190','down':u'\u2192'},
    'ioarrow':{'up':'x','down':u'\2299'},
    'photon':{'up':u'\u21b7','down':u'\u21b6'},
    'pm':{'up':'+','down':'-'},
    'N':'N',
}

#base_repr = base_reprs['ud']

def set_base_repr(arg):
    '''Set the printout of the base representation:
    
    :param str arg: 'ud', '01', or 'arrow'
    
    These will cause state vector bases to be printed out as,
    for example
    
    - `arg = 'ud'` results in :math:`\\Ket{u}`, :math:`\\Ket{ud}`, etc.
    - `arg = '01'` results in :math:`\\Ket{0}`, :math:`\\Ket{01}`, etc.
    - `arg = 'arrow'` results in :math:`\\Ket{\\uparrow}`, :math:`\\Ket{\\uparrow\\downarrow}`, etc.
    
    '''
    global base_repr,base_repr_key
    if arg not in base_reprs:
        assert isinstance(arg,str)
        assert len(arg) == 2
        base_reprs[arg] = {'up':arg[0],'down':arg[1]}
    base_repr = base_reprs[arg]
    state.base_repr = arg

def create_basis(n,kind='ket',base_repr=None):
    '''create a basis set for n particles
    '''
    if base_repr is None:
        base_repr = base_reprs[state.base_repr]
    else:
        if base_repr not in base_reprs:
            assert isinstance(base_repr,str)
            assert len(base_repr) == 2
            base_reprs[base_repr] = {'up':base_repr[0],'down':base_repr[1]}
        base_repr = base_reprs[base_repr]
    if isinstance(base_repr,dict):
        s1 = [base_repr['up'],base_repr['down']]
        s = copy.copy(s1)
        for k in range(n-1):
            s = [x+y for x,y in list(itertools.product(s,s1))]
    elif isinstance(base_repr,str) and base_repr == 'N':
        N = 2**n
        s = [str(k) for k in range(N)]
    if kind=='ket': s = ['|%s>'%x for x in s]
    else: s = ['<%s|'%x for x in s]
    return s

basis = lambda n: [ket(x,n=n) if state.base_repr == 'N' else ket(x) for x in create_basis(n)]

class state(object):
    '''Quantum state
    
    A quantum state has the property of being either a bra or a ket vector.
    States `a` and `b` can
    
    - Inner product: `a.H*b` = :math:`\\Braket{a | b}`
    - Kronecker product: (expand the number of particles in the state) `a**b` = :math:`\\Ket{a} \\otimes \\Ket{b} = \\Ket{ab}`
    - Hermetion transpose: (convert bra to ket and vice-versa) `a.H` does either
      :math:`\\Ket{a} \\rightarrow \\Bra{a}` or :math:`\\Bra{a} \\rightarrow \\Ket{a}`
    - Normalize: `a.N` = :math:`\\frac{1}{\\sqrt{\\Braket{a|a}}} \\Ket{a}`
    - Access the wave function as a column vector: `a.phi` = :math:`[\\Braket{a|e_i}]` where :math:`\\Ket{e_i}` are the basis vectors
    
    '''
    
    def __init__(self,s,n=0,name=''):
        '''initialize with either a basis ket, or a wave function
        
        arguments:
            s
              string - the string representation of a basis state, such as '|0>' or '|5>'
              ndarray or list - the wave function, a vector of complex numbers
                                representing the state (this doesn't need to be a basis state).
            n int the number of particles, in case this can't be determined from s
            name - (string) an optional name of the state
        '''
        assert isinstance(s,(str,unicode,np.matrix,list))
        self.name = name # the default
        named = True if len(name) > 0 else False
        self.kind = 'ket' # the default
        
        if isinstance(s,list):
            s = np.matrix(s).T # default is ket, so make the wave function a column vector

        if isinstance(s,np.matrix):
            if s.dtype not in (float,complex):
                s = s.astype(float)
            self.n = N = s.size
            self.phi = s.copy()
            self.n_particles = n_particles = int(np.log2(N))
            if self.phi.shape == (1,N): self.kind = 'bra'
            elif self.phi.shape == (N,1): self.kind = 'ket'
            else: raise Exception('wave function must be a row or column vector')
            self.basis = create_basis(n_particles,kind=self.kind)
            
        elif isinstance(s,(str,unicode)):
            if not named: self.name = s
            if n>0:
                assert base_repr == 'N'
                self.n_particles = n_particles = n
            else:
                if isinstance(base_repr,dict):
                    self.n_particles = n_particles = len(s.strip('<|>'))
                else:
                    raise Exception(f'n (number of particles) must be specified with an |N> state')
            if s.startswith('<') and s.endswith('|'): # make it a bra
                self.kind = 'bra'
            elif s.startswith('|') and s.endswith('>'): # make it a ket
                self.kind = 'ket'
            else:   # make it a ket
                s = '|%s>'%s
                if not named: self.name = s
                self.kind = 'ket'
            self.basis = create_basis(n_particles,kind=self.kind)
            assert s in self.basis, '%s has to be in basis %r'%(s,self.basis)
            k = self.basis.index(s)
            self.n = N = len(self.basis)
            phi = [0.]*N # the wave function
            phi[k] = 1.
            self.phi = np.matrix(phi)
            if self.kind == 'ket': self.phi = self.phi.T
        
    def to_bra(self):
        '''convert ket to bra
        '''
        if self.isbra:
            return self
        self.phi = self.phi.H
        if hasattr(self,'name') and self.name in self.basis:
            self.name = '<'+self.name[1:-1]+'|'
        self.basis = ['<'+s[1:-1]+'|' for s in self.basis]
        self.kind = 'bra'
        return self
        
    def to_ket(self):
        '''convert bra to ket
        '''
        if self.isket:
            return self
        self.phi = self.phi.H
        if hasattr(self,'name') and self.name in self.basis:
            self.name = '|'+self.name[1:-1]+'>'
        self.basis = ['|'+s[1:-1]+'>' for s in self.basis]
        self.kind = 'ket'
        return self
        
    def __unicode__(self):
        ''' string representation of state
        '''
        if printoptions.sqrtpi:
            orepr = lambda z: ('+'+mprepr(z,onaxis=True)).replace('+-','-')
        else:
            orepr = lambda z: ('+'+crepr(z,plus_sign=True)).replace('+-','-').replace('++','+')
        #with np.printoptions(precision=_prec,suppress=_sup,formatter={'all':crepr}):
        with np.printoptions(precision=printoptions.prec,suppress=printoptions.sup,formatter={'all':orepr}):
            #phi = [crepr(c,plus_sign=True) for c in flatten(self.phi.tolist())]
            phi = [orepr(c) for c in flatten(self.phi.tolist())]
        phi = [('%s %s'%(c[0],c[1:]),b) for c,b in zip(phi,self.basis) if c[1:] != '0']
        phi = [(c[0],b) if c[2:] in ['1','1i'] else (c,b) for c,b in phi]
        phi = ['%s %s'%e for e in phi]
        s = (' '.join(phi)).lstrip('+ ').strip()
        if s == '': s = '0'
        return s
    
    def __repr__(self):
        s = self.__unicode__()
        if sys.version_info.major == 2: return s.encode('utf-8')
        return s
    
    def __add__(self,another):
        """|a> + |b>
        """
        if isinstance(another,(int,float)):
            if another == 0: return self
            else:
                raise NotImplementedError("unsupported operand type for +: scalar !=0 and 'state'")
        #assert isinstance(another,self.__class__)
        elif isinstance(another,self.__class__):
            assert self.kind == another.kind
            return self.__class__(self.phi + another.phi)
        else: return NotImplemented

    def __radd__(self,another):
        '''take care of the special case of adding a state to 0
        '''
        if another == 0: return self
        else: raise NotImplementedError("unsupported operand type for +: scalar !=0 and 'state'")
    
    def __sub__(self,another):
        """|a> - |b>
        """
        #assert isinstance(another,self.__class__)
        if isinstance(another,self.__class__):
            assert self.kind == another.kind
            return self.__class__(self.phi - another.phi)
        else: return NotImplemented
    
    def __mul__(self,another):
        """
        inner product: <a|b>
        operation on bra: <a|A
        scalar product: <a| alpha or <a| alpha
        outer product: |a><b|
        """
        if debug: print ('state.__mul__')
        #assert isinstance(another,(int,float,complex,self.__class__,operator,str))
        if isinstance(another,self.__class__): # either inner or outer product
            if self.isbra: # <a|b>
                assert not another.isbra,'cannot multiply two bras'
                r = (self.phi*another.phi).item(0)
                if isinstance(r,complex):
                    if r.imag == 0:
                        r = r.real
                        return qfloat(r)
                    else:
                        return qcomplex(r)
                else:
                    return qfloat(r)
            else: # |a><b|
                assert another.isbra,'cannot multiply two kets'
                return operator(self.phi*another.phi)
        elif isinstance(another,operator): #  <a|A
            assert self.isbra
            phi = (self.phi * another.op)
            return bra(phi)
        elif isinstance(another,str): # '<a|'*A
            another = self.__class__(another)
            return self*another
        elif isinstance(another,(int,float,complex)):
            if self.isbra:
                r = bra(self.phi*another) #  <a| alpha
            else:
                r = ket(self.phi*another) #  |a> alpha
            return r
        else: return NotImplemented

    def __rmul__(self,another):
        """scalar product alpha |a> or alpha <a|
        or product of states with strings: '<u|'*b or '|u>'*b
        """
        if debug: print ('state.__rmul__')
        #assert isinstance(another,(Number,str))
        if isinstance(another,Number):
            return self.__mul__(another)
        elif isinstance(another,str):
            if '|' not in another and self.isket:  # make it the opposite kind as self to allow multiplication
                a = bra(another)
            else:
                a = state(another)
            return a.__mul__(self)
        else: return NotImplemented

    def __or__(self,other):
        '''a state or operator multiplication operator, like a | b or A | u
        '''
        return self*other
    
    def __ror__(self,other):
        return other*self
        
    def __div__(self,other):
        """scalar product |a> / alpha
        If both arguments are states, this is a test of "divides", returning (True/False,byfactor)
        """
        if isinstance(other,state): # check if one state is a multiple of another
            return self.divides(other)
        #assert isinstance(other,(int,float,complex))
        if isinstance(other,(int,float,complex)):
            r = self.copy()
            r.phi = self.phi/other
            return r
        else: return NotImplemented

    def divides(self,other):
        #assert isinstance(other,state)
        if isinstance(other,state):
            a = self.phi.flat
            b = other.phi.flat
            q = None
            l = []
            for aa,bb in zip(a,b):
                if aa == 0 and bb == 0:
                    l.append(True)
                elif bb == 0:
                    l.append(False)
                else:
                    f = aa/bb
                    if q is None:
                        q = f
                    l.append(q == f)
            r = np.array(l).all()
            if r:
                return(r,q)
            else:
                return(r,None)
        else: return NotImplemented
    
    def __truediv__(self,num):
        """scalar product |a> / alpha
        """
        #assert isinstance(num,(int,float,complex))
        if isinstance(num,(int,float,complex)):
            if self.isbra:
                r = bra(self.phi/num) #  <a| alpha
            else:
                r = ket(self.phi/num) #  |a> alpha
            return r
        else: return NotImplemented

    def __neg__(self):
        return (-1)*self #   - |a>
    
    def __invert__(self):
        '''used to convert a bra to a ket or vice-versa
        as in ~s. Equivalent to s.H
        '''
        return self.H
    
    def __getitem__(self,*args):
        '''get the state of the k'th particle
        (particles are numbered 0,1,2,...,n_particles-1)
        
        k can be a list of particle numbers in which case
        the result is the state of that particle set
        
        k can also be a slice, e.g. [1:3] or [1:5:2] ([start:stop:step])
        '''
        n = self.n_particles
        k = args[0]
        
        if isinstance(k,tuple):
            k = list(k)
        
        if isinstance(k,int):
            assert k>=0 and k<n
            phi = self.phi.T.tolist()[0]
            basis = self.basis #bases[n]
            q = [(c*ket(b.strip('<|>')[k])) for c,b in zip(phi,basis)]
            r = sum(q).N
        elif isinstance(k,list):
            assert all([isinstance(x,int) and x>=0 and x<n for x in k])
            b = self._aslist()
            basis = create_basis(len(k))
            at = [x.strip('<|>') for x in basis]
            q = [[(c,s) for c,s in b if ''.join([s[x] for x in k]) == ai] for ai in at]
            r = [(sum([c for c,s in x]),b) for x,b in zip(q,at)]
            r = [c*ket(s) for c,s in r]
            r = sum(r).N
            return r
        elif isinstance(k,slice):
            start,stop,step = k.start,k.stop,k.step
            if start is None: start = 0
            if stop is None: stop = n
            if step is None: step = 1
            ks = list(range(start,stop,step))
            r = self[ks]
        return r

    @property
    def isket(self):
        return self.kind == 'ket'
    
    @property
    def isbra(self):
        return self.kind == 'bra'
    
    @property
    def H(self):
        '''Hermetian conjugate
        '''
        if self.isbra:
            return self.copy().to_ket()
        else:
            return self.copy().to_bra()

    @property
    def N(self):
        '''Normalized
        '''
        return self.normalized()
        
    def __pow__(self,other):
        '''kroneker product
        '''
        return self.kron(other)
    
    def __xor__(self,other):
        '''kroneker product (alternative syntax)
        '''
        return self.kron(other)
    
    def __eq__(self,other):
        assert isinstance(other,self.__class__)
        if self.kind != other.kind: return False
        if self.phi.shape != other.phi.shape: return False
        return (np.isclose(self.phi,other.phi)).all()
    
    def _aslist(self):
        '''report the state as a list of tuples of (coefficient,basis)
        
        For example, s = ket('0') is
        s._aslist() = [(1.0, '0'), (0.0, '1')]
        '''
        return [(c,x.strip('<|>')) for x,c in zip(self.basis,flatten(self.phi.tolist()))]
    
    def copy(self):
        return copy.deepcopy(self)
    
    def measure(self,k='all',return_obs=False):
        '''measure the state in the 'computational' basis (Z is the observable).
        If the state consistest of more than one particle, measure all particles
        sequentially.
        
        Return the final ('collapsed') state and a list of the observations.
        
        k is an optional particle number, indicating that only this
        particle is measured.
        
        k can be a list of particle numbers
        
        particles are numbered 0,1,2,...,n_particles-1
        '''
        n = self.n_particles
        if k == 'all': k = list(range(n))
        if isinstance(k,int): k = [k]
        assert all([j>=0 and j<n for j in k])
        s = self
        r = []
        for j in k:
            v,s = (I(j)^Z^I(n-j-1)).measure(s)
            r.append(v)
        if return_obs:
            return s,r
        else:
            return s
    
    def norm(self):
        '''return the norm of the wave function
        '''
        if self.isbra:
            norm2 = (self.phi*self.phi.H).item(0)
        else:
            norm2 = (self.phi.H*self.phi).item(0)
        return sqrt(norm2).abs
    
    def normalize(self):
        """normalizes the wave function
        """
        self.phi /= self.norm()
    
    def normalized(self):
        """returns the normalized version
        """
        norm = self.norm()
        if np.isclose(norm,0): return self
        return self/norm
    
    def density(self):
        """generate the density matrix
        :math:`\\rho = \\ket{s}\\bra{s}`
        """
        return (ket(self)*bra(self))
    
    def _index(self):
        '''makes an index from string representations of basis states to
        integer offset within the state vector
        '''
        s1 = ['u','d']
        s = copy.copy(s1)
        for k in range(self.n_particles-1):
            s = [x+y for x,y in list(itertools.product(s,s1))]
        
        d = dict(zip(s,range(self.n)))
        return d
    
    def density1(self,particle_no=None):
        '''generate the density matrix for one
        of the particles, `particle_no`
        Particle numbers are 0...n_particles-1
        '''
        if particle_no == None: # old style call
            return (self.density1(0),self.density1(1))
        assert particle_no < self.n_particles
        d = self._index()
        p = particle_no
        pstr = '_'
        rho = self.density().matrix
        s1 = ['u','d']
        keys = sorted(d.keys())[::-1] # 'uuu','uud',...
        f = set([key[:p]+pstr+key[p+1:] for key in keys]) # 'u_u','u_d'...
        ind_keys_u,ind_keys_d = [[ff.replace(pstr,s) for ff in f] for s in s1] # [['uuu','uud'...],['udu','udd'...]]
        rho_p = np.matrix(np.zeros((2,2)))
        for z in zip(ind_keys_u,ind_keys_d):
            ind2x2 = [[(z0,z1) for z0 in z] for z1 in z] # [[('dud', 'dud'), ('ddd', 'dud')], [('dud', 'ddd'), ('ddd', 'ddd')]]
            for i in range(2):
                for j in range(2):
                    ii,jj = ind2x2[i][j]
                    rho_p[i,j] += rho[ d[ii],d[jj] ]
        return rho_p

    def correlation(self):
        '''compute the :math:`n_{particles}\\times n_{particles}` correlation matrix
        '''
        n_particles = self.n_particles
        u = ket([1,0])
        d = ket([0,1])
        Sz = u*u.H - d*d.H
        S0 = u*u.H + d*d.H # operator(np.identity(2))
        c = np.zeros((n_particles, n_particles))
        A = []
        for p1 in range(n_particles):
            Ap = (Sz if p1==0 else S0)
            for p2 in range(1,n_particles):
                Ap = Ap**(Sz if p2==p1 else S0)
            A.append(Ap)
        for i in range(n_particles):
            for j in range(n_particles):
                c[i,j] = (A[i]*self).H*(A[j]*self)
        return c

    @property
    def C(self):
        return self.correlation()
    
    def prob(self,*args):
        """determine the probability of collapsing to this state from an
        initial state s (optionally, after the measurement A).
        
        If only one argument is given, it is the initial state, s
        
        self.prob(A,s) - A is the obervable (operator), s is the initial state
            a.prob(A,s) = (a.H*A*s)**2 =` :math:`\\Braket{a | A | s}^2`
            
        self.prob(s) - s is the starting state:
            a.prob(s) = (a.H*s)**2 =` :math:`\\Braket{a | s}^2`
        
        """
        if len(args) == 2:
            A,s = args
            if isinstance(s,str): s = ket(s)
            return qfloat(np.abs(s.H*A*self)**2)
        else:
            s = args[0]
            if isinstance(s,str): s = ket(s)
            return qfloat(np.abs(s.H*self)**2)

    def probs(self,endstates='basis'):
        '''calculates the probabilities of collapse to each possible end state.
        endstates is a list of states, defauting to the basis states.
        '''
        if isinstance(endstates,list):
            basis = endstates
        else:
            basis = self.basis
        return [t for t in [(self.N.prob(ket(x)),ket(x)) for x in basis] if t[0]>0]
        
    def kron(self,another):
        if isinstance(another,self.__class__):
            assert self.kind == another.kind
            r = state([0.]*self.n*another.n)
            r.phi = np.kron(self.phi,another.phi)
            return r
        else: return NotImplemented

    def entangled(self):
        '''determine if a 2-particle pure state is entangled.
        This works only for n=2 particle states.
        
        :return: True or False
        
        '''
        assert self.n_particles == 2
        S = entropy(ptrace(self.density(),[0]))
        if np.isclose(S,0.):
            return False
        else:
            return True

set_base_repr('ud')

def ket(s,n=0):
    """generate a state vector given the state as a string or a wave function
    or generate the ket version of a bra vector
    
    :param s: specifier. If `s` is a:
    
    - :class:`str`: generate one of the basis states (e.g. `u`, `d`, `uu`, `ud`, `uud`, etc.)
    - :class:`numpy.ndarray`: generate state from the wave function vector
    - :class:`state`: convert the state (bra or ket) to a ket
    
    :return: :math:`\\ket{s}`
    :rtype: :class:`state`

    """
    if isinstance(s,str):
        return ket( state(s,n=n) )
    elif isinstance(s,(np.matrix,list)):
        return ket( state(s) )
    elif isinstance(s,state):
        if s.isket:
            return s
        else:
            return s.copy().to_ket()
    raise Exception('%r is not a valid state'%s)

def bra(s,n=0):
    """generate a state vector given the state as a string or wave function
    or generate the bra version of a ket vector

    :param s: specifier. If `s` is a:
    
    - :class:`str`: generate one of the basis states (e.g. `u`, `d`, `uu`, `ud`, `uud`, etc.) as a bra
    - :class:`numpy.ndarray`: generate state from the wave function vector
    - :class:`state`: convert the state (bra or ket) to a bra
    
    :return: :math:`\\bra{s}`
    :rtype: :class:`state`

    """
    if isinstance(s,str):
        return bra( state(s,n=n) )
    elif isinstance(s,(np.matrix,list)):
        return bra( state(s) )
    elif isinstance(s,state):
        if s.isbra:
            return s
        return s.copy().to_bra()
    else:
        raise Exception('% is not a valid state'%s)

def density(p,s):
    '''Create the mixed state density matrix, :math:`\\rho` given
    a set of probabilities and a list of pure states.
    
    .. math::
    
        \\rho = \\sum_i p_i \\Ket{s_i} \\bra{s_i}
    
    :param list p: the list of probabilities :math:`p_i`
    :param list s: the list of pure states :math:`\\bra{s_i}`
    
    The probabilities must all be between zero and one and sum to one.
    
    :rtype: :class:`numpy.matrix`
    
    '''
    p = np.array(p)
    assert ((p>=0) &(p<=1)).all()
    assert np.isclose(p.sum(),1.0)
    assert len(p) == len(s)
    rho = 0
    for p_i,psi_i in zip(p,s):
        rho += p_i*psi_i.density()
    return rho

def entropy(rho,frac=False,decohere=False):
    """Calculate the Von Neumann entropy given the density matrix.
    
    .. math::
    
        S = -\\rm{tr}\\left(\\rho\, \\log(\\rho)\\right)
        
    where :math:`\\rho` is the density matrix.
    
    :param numpy.matrix rho: the density matrix
    :param bool frac: If `frac=True`, return :math:`S/S_{max}` where :math:`S_{max} = \\log(n)` the maximum entropy.
    :param bool decohere: If decohere=True then assume decoherent population (off-diagonal elements of :math:`\\rho` set equal to zero)
    
    :return: entropy, :math:`S`
    :rtype: float
    
    """
    if isinstance(rho,operator): rho = rho.op
    if decohere:
        w = np.diag(rho)
    else:
        w,v = np.linalg.eig(rho)
    n = w.size
    S = 0
    for k in range(n):
        if w[k] > 0 and not np.isclose(w[k],1.0):
            S -= w[k]*np.log(w[k])
    if frac:
        S_max = np.log(float(n))
        return qfloat(S/S_max)
    else:
        return qfloat(S)

def ptrace(rho,ps):
    '''**Partial Trace**: generate the density matrix with specified particles traced out.
    
    :param np.matrix rho: the multi-particle density matrix
    :param list ps: list of particles to trace out; given by number 0,1...n_particles-1
    
    If the list is empty, return the full density matrix.
    If the list has all the particles, then returns the trace of the full density matrix (always = 1)
    The list must have unique particle numbers which are all < n_particles
    
    :rtype: :class:`numpy.matrix`
    
    See: https://en.wikipedia.org/wiki/Partial_trace
    '''
    if len(ps) == 0: return rho
    if isinstance(rho,operator): rho = rho.op
    n_particles = int(np.log2(rho.shape[0]))
    n = n_particles
    assert np.array([x < n for x in ps]).all(),'all elements of ps %r must be < number of particles %r'%(ps,[x < n for x in ps])
    assert np.unique(ps).size == len(ps),'elements of ps must be unique: %r'%ps
    ps = sorted(ps)
    nr = 2**len(ps)
    nk = 2**(n - len(ps))
    n = 2**n
    if nk == 1: return operator(1)
    d,dr,dk = [state([1]*x)._index() for x in [n,nr,nk]]
    di,dri,dki = [sorted(x.keys(),reverse=True) for x in [d,dr,dk]]
    Ms = [ [[ (_mosh(x,z,ps),_mosh(y,z,ps)) for x in dki] for y in dki] for z in dri]
    Msv =  [[[rho[ d[x], d[y] ] for (x,y) in row ] for row in mat ] for mat in Ms ]
    den = np.sum( [ np.array(x) for x in Msv], axis=0 )
    #return np.matrix(den)
    return operator(den)

def Cab(rho):
    '''calculate the 'concurrence' of a two particle system.
    ref: Coffman, V., Kundu, J., & Wootters, W. K. (2000).
    Distributed Entanglement. Physical Review A, 61(5), 5–9.
    http://doi.org/10.1103/PhysRevA.61.052306
    
    :param np.matrix rho:
    
    Presently this only calculates on a two particle system (4x4 density matrix)
    '''
    if isinstance(rho,operator): rho = rho.op
    assert rho.shape == (4,4)
    sy = operator('sy')
    rho_tilde = ((sy**sy)*(rho.conj())*(sy**sy)).op
    lam = np.linalg.eig(rho*rho_tilde)[0].real
    lam = np.clip(lam,0,np.inf)
    lam = np.sqrt(np.sort(lam))[::-1]
    cab = np.max([lam[0]-lam[1]-lam[2]-lam[3],0])
    return qfloat(cab)

def _mosh(s1,s2,ps):
    '''mosh string 1 with string 2 putting string 2 characters in posisions ps
    '''
    n = len(s1) + len(s2)
    psk = [ x for x in range(n) if x not in ps ]
    s = np.array(['']*n)
    s[ps] = list(s2)
    s[psk] = list(s1)
    return ''.join(list(s))

#============== Quantum Operators (Observables) ==============
class operator(object):
    '''
    Quantum operator
    
    Operators can
    
    - Multiply :class:`state`, `A*s` = :math:`A \\Ket{s}`
    - Multiply each other, `A*B` = :math:`A\\times B`
    - Kronecker-product `A**B` = :math:`A\\otimes B`.

    Also
    
    - `A.H` is the Hermetian transpose: :math:`A^H`
    - `A.N` is normalization, so that :math:`\\lvert det(\\bar A) \\rvert = 1`

    :param numpy.ndmatrix A: Initialize with a square, Hermetian matrix `A`
    '''
    ops = ['s0','sx','sy','sz']
    '''a list of operators that can be specified by a string argumant to :class:`operator`'''
    
    sig_0 = np.matrix([[1,0],[0,1]])
    sig_x = np.matrix([[0,1],[1,0]])
    sig_y = np.matrix([[0,-i],[i,0]])
    sig_z = np.matrix([[1,0],[0,-1]])
    op_mats = [sig_0,sig_x,sig_y,sig_z]

    @property
    def matrix(self):
        '''returns the matrix representation of the operator
        '''
        return np.matrix(self.op)
    
    def __init__(self, A,*args):
        '''initialize given a square, Hermetian matrix A
        '''
        if debug: print ('operator.__init__')
        if not isinstance(A,(str,np.matrix)):
            if isinstance(A,operator): A = A.op
            elif isinstance(A,np.ndarray) or np.isscalar(A): A = np.matrix(A)
            else: raise Exception(f'unsupported data type {type(A)}')
        assert isinstance(A,(str,np.matrix))
        if isinstance(A,str):
            assert A in self.ops
            self.name = A
            k = self.ops.index(A)
            self.op = A = self.__class__.op_mats[k]
        n,m = A.shape
        assert n == m
        self.op = A
        self.n_particles = n_particles = int(np.log2(n))
        self.basis = create_basis(n_particles)

    def __repr__(self):
        sep = ' ' # row element separator, e.g. ','
        ttbl = {'[':'', ']':''}
        ttbl = str.maketrans(ttbl)
        if printoptions.sqrtpi:
            orepr = lambda z: mprepr(z,onaxis=True)
        else:
            orepr = lambda z: crepr(z).lstrip('+')
        def just(q,n): # center the elements but shift left by one space if negative sign
            return q.center(n)[1:]+' ' if q[0]=='-' and not len(q)%2 else q.center(n)
        #with np.printoptions(suppress=_sup,precision=_prec,formatter={'all':orepr}):
        with np.printoptions(suppress=printoptions.sup,precision=printoptions.prec,formatter={'all':orepr}):
            s = [x.split() for x in str(self.op).translate(ttbl).split('\n')]
        n = max([len(el) for row in s for el in row])
        s = [[just(x,n+2)[:-1] for x in row] for row in s]
        s = '[['+' ]\n ['.join([sep.join(x) for x in s])+' ]]'
        nz = getattr(self,'_nz',False)
        if printoptions.nz or nz:
            s = s.replace(' 0 ','   ').replace('-0 ','   ')
            self._nz = False
        return s
    
    @property
    def nz(self):
        '''Suppress '0's from the printout of the operator matrix
        (making it easier to see non-zero entries).
        '''
        self._nz = True
        return self

    def __mul__(self,another):
        """
        multiplication on the left: operator*another
        scalar multiplication: A*alpha
        operation on ket: A |a>
        operator x operator: A*B
        operator x matrix: A*sig
        operator x string (basis state ket): A*'|u>'
        """
        if debug: print ('%r.__mul__(%r)'%(self.__class__,type(another)))
        if isinstance(another,(int,float,complex)): # alpha A
            return self.__class__(self.op*another)
        elif isinstance(another,state): # A |a>
            assert another.isket
            return state(self.op*another.phi)
        elif isinstance(another,operator):  # A*B
            return self.__class__(self.op*another.op)
        elif isinstance(another,np.matrix): # assume this operator*density_matrix
            return self.__class__(self.op*another)
        elif isinstance(another,str): # convert the string to a basis state
            return self*ket(another)
        else: return NotImplemented

    def __rmul__(self,another):
        """
        multiplication on the right: another*operator
        scalar multiply : A*alpha
        by bra: <a| A
        by matrix: sig*A
        by string (basis state bra): '<a|'*A
        """
        if debug: print ('%r.__rmul__(%r)'%(self.__class__,type(another)))
        if isinstance(another,(int,float,complex)):
            return self.__class__(self.op*another) #  A*alpha
        elif isinstance(another,state):
            return state(another.phi*self.op) # <a| A
        elif isinstance(another,np.matrix):
            return self.__class__(another*self.op) # sig*A
        elif isinstance(another,str):
            if '|' not in another:
                a = bra(another)
            else:
                a = state(another)
            return a*self # convert the string to a basis state
        else: return NotImplemented
    
    def __or__(self,another):
        return self*another
    
    def __ror__(self,another):
        return another*self
    
    def __add__(self,another):
        """ A+B
        """
        if isinstance(another,(int,float)):
            if another == 0: return self
            else: raise NotImplementedError("unsupported operand type for +: scalar !=0 and 'operator'")

        if isinstance(another,operator):
            return self.__class__(self.op+another.op) # A + B
        else:
            return NotImplemented
    
    def __radd__(self,another):
        '''take care of the special case of adding an operator to 0
        '''
        if another == 0: return self
        else:
            #raise NotImplementedError("unsupported operand type for +: scalar !=0 and 'state'")
            return NotImplemented

    def __sub__(self,another):
        """ A-B
        """
        if isinstance(another,operator):
            return self.__class__(self.op-another.op) #  A - B
        else:
            return NotImplemented
    
    def __div__(self,num):
        """
        divide by a scalar A/alpha
        """
        if isinstance(num,(int,float,complex)):
            return self.__class__(self.op/num)
        else: return NotImplemented
        
    def __truediv__(self,num):
        """
        divide by a scalar A/alpha
        """
        if isinstance(num,(int,float,complex)):
            return self.__class__(self.op/num)
        else: return NotImplemented
        
    def __neg__(self): # -A
        """
        scalar negation -A
        """
        return (-1)*self

    def __eq__(self,other):
        if isinstance(other,self.__class__):
            if self.n_particles != other.n_particles: return False
            return np.isclose(self.op,other.op).all()
        else: return NotImplemented
    
    def __pow__(self,other):
        if np.isscalar(other):
            if isinstance(other,complex):
                if np.isclose(other,other.real):
                    other = other.real
                else:
                    return gate(expm(other*logm(self.op)))
                    #return sum([(v**other)*e*e.H for v,e in self.eig()])
            if isinstance(other,int):
                return gate(matrix_power(self.op,other))
            else:
                return gate(fractional_matrix_power(self.op,other))
            #return sum([(v**other)*e*e.H for v,e in self.eig()])
            #return self.__class__(self.matrix**other)
        elif isinstance(other,operator):
            return self.kron(other)
        elif other is None:
            return self
        else:
            #raise Exception(f'operator ** {other.__class__} not supported')
            return NotImplemented

    def __rpow__(self,other):
        return self
    
    def __xor__(self,other):
        return self.__pow__(other)

    def __rxor__(self,other):
        return self
    
    def __getitem__(self,args):
        j,k = args
        if self.op.dtype in (int,float):
            return qfloat(self.op[j,k])
        elif self.op.dtype in (complex,):
            return qcomplex(self.op[j,k])
        else:
            raise Exception('failed to get int,float,or complex item')
    
    def __setitem__(self,args,v):
        j,k = args
        self.op[j,k] = v
        
    def trace(self):
        '''trace (sum of diagonal elements) of the operator
        '''
        return qcomplex(np.trace(self.matrix))
    
    @property
    def H(self):
        '''Hermetian conjugate'''
        r = self.copy()
        r.op = self.op.conj().T
        return r

    def __invert__(self):
        '''conjugate transpose. Equivalent to .H
        '''
        return self.H
    
    @property
    def N(self):
        '''Normalized'''
        return self.normalized()
    
    def copy(self):
        return copy.deepcopy(self)
    
    def kron(self,other):
        if other is None:
            return self
        elif isinstance(other,operator):
            return self.__class__(np.kron(self.op,other.op))
        else:
            return NotImplemented
    
    def normalized(self):
        '''return the normalized version of the operator
        such that :math:`\\lvert det(\\bar A) \\rvert = 1`.
        '''
        norm = np.sqrt(np.abs(np.linalg.det(self.matrix)))
        if np.isclose(norm,0.):
            raise Error('operator is close to singular, cannot normalize')
        return self/norm
    
    def measure(self,s):
        '''produce the random measurement using the operator as an observable
        on a particle in state s.
        
        This produces a number equal to one of the eigenvalues
        v_i of `A` according to a probability distribution
            [Prob(i)] = [ | < e_i | s > |^2 ]  i=1,...,2^n_particles
        where e_i are the eigenstates of A,
        and collapses the wave function to the corresponding eigenstate | e_i >
        
        Returns the tuple (observed value, collaped state) =
        ( v_i, e_i )

        In the case of a degenerate eigenspectrum (repeated eigenvalues of A),
        the collapse is into the subspace of eigenstates with that
        eigenvalue according to
        
            P | s > / sqrt( < s | P | s >)
        
        where P is the projection operator sum_k | e_k >< e_k | where e_k is the
        set of eigenvectors with the repeated eigenvalue.
        
        Argument:
            s :class:`state`
            
        See:
            https://en.wikipedia.org/wiki/Mathematical_formulation_of_quantum_mechanics#Postulates_of_quantum_mechanics
        '''
        s = ket(s)
        n = s.n
        assert n == self.op.shape[0]
        r = self.eig()
        P = [ abs2(v.H*s) for v in self._eigenstates ] # postulate II.b
        k = np.random.choice( range(n), p=P )
        # take care of repeated eigenvalues, if any:
        lam = r[k][0]
        proj = sum([ (e*e.H)*s for v,e in r if v == lam]) # postulate II.c
        rv = (lam,proj.N)
        return rv
    
    def probs(self,rho):
        '''calculate the probabilities for each outcome of the measurement
        operator given the density matrix of the system of particles.
        
        Returns a list of tuples (x_i, e_i, prob_i) where x_i is the value of the
        observable, e_i is the eigenstate, and prob_i is the probability of that eigenstate.
        (Note that repeated eigenvalues are not combined)
        '''
        self.eig()
        P = [trace(e*e.H*rho) for e in self._eigenstates]
        r = list(zip(self._observables,self._eigenstates,P))
        return r
        
    def average(self,s):
        '''calculate the average value of the observable for a given pure state s
        or, for (in general) a mixed state, given the density matrix s
       
            < A > = < s | A | s >  (s is a state) or
            < A > = tr ( s A )     (s is a density matrix)
        
        Arguments:
            s :class:`state` or :class:`ndarray` or :<class>:`operator`
        
        '''
        if isinstance(s,state):
            rho = s.density()
        elif isinstance(s, np.ndarray):
            rho = operator(s)
        elif isinstance(s, operator):
            rho = s
        else:
            raise Exception('argument must be a state or a density matrix')
        return trace(rho*self)
    
    def mean(self,s):
        return self.average(s)
    
    def var(self,s):
        '''calculate the variance of the observable for a given
        pure state, or for (in general) a mixed state, given the density matrix
        
            var(A) = < s | A*A | s >  - < A >^2  (s is a state) or
            var(A) = tr( s A*A ) - < A >^2       (s is a density matrix)

        Arguments:
            s :class:`state` or :class:`ndarray` or :<class>:`operator`
        
        '''
        if isinstance(s,state):
            rho = s.density()
        elif isinstance(s, np.ndarray):
            rho = operator(s)
        elif isinstance(s, operator):
            rho = s
        else:
            raise Exception('argument must be a state or a density matrix')
        mean = trace(rho*self)
        return qcomplex(trace(rho*self*self) - mean**2)

    def eig(self):
        '''calcluate the eigenvalues and eigenvectors of the operator.
        Returns an array of eigenvalue, eigenvector pairs
        '''
        n = self.op.shape[0]
        if not hasattr(self,'_eigenstates'):
            ev,evec = np.linalg.eig(self.matrix)
            self._observables = [qcomplex(x) for x in ev.tolist()]
            self._eigenstates = [ket(x) for x in evec.T.tolist()]
        return list(zip(self._observables,self._eigenstates))
    
    @property
    def observables(self):
        return [x[0] for x in self.eig()]
    
    @property
    def eigenstates(self):
        return [x[1] for x in self.eig()]
    
def measure(A,s):
    '''measure the observable A given the system is in state s.
    Returns a tuple (observed value, collapsed eigenstate)
    '''
    assert isinstance(A,operator)
    assert isinstance(s,state)
    return A.measure(s)

def average(A,s):
    return A.average(s)

def mean(A,s):
    return A.average(s)

def var(A,s):
    return A.var(s)

def trace(A):
    '''trace of an operator A
    '''
    return A.trace()

def eig(A):
    ''' calculate the eigenvalues (observables) and eigenstates
    of the operator A. Returns a list of eigenvalue,eigenstate pairs
    '''
    return A.eig()

#============ Quantum Gates =============
class gate(operator):
    '''Quantum gate, a subclass of :class:`operator`
    
    Generate a gate with a constructor call:
    
        g = **gate** (*name*)
    
    where name is one of the names in the list `gate.gates`. These include 'Hadamard','NOT',  etc.
    One can also force any operator to be a gate if the name is one of the pre-defined operators
    (given in the `operator.ops` list), or with:
    
        g = **gate** (*matrix* | *operator*, [name = *name*])
    
    Some gates, `'Rz'` and `'XX'` need an argument, the angle :math:`\\phi`. These are
    created by passing `phi` as the second argument:
    
        g = **gate** (*name*, phi = *phi*)
    
    One can create a 'controlled' gate by preceding the name of the gate
    (which must be in the gate.gates list) with a lower case c, as in
    
        g = **gate** ('cNOT')
        
    One can create a 'square root' gate by preceding the name of the gate
    (which must be in the gate.gates list) with a lower case s, as in
    
        g = **gate** ('sNOT')
    '''
    gates = ['H','Hadamard','X','Y','Z','I','P','Rx','Ry','Rz','NOT','S','Sjk','SWAP','iSWAP','T',
             'XX','YY','ZZ','XY','YZ','ZX','YX','ZY','XZ']
    '''A list of the predefined gates that can be specified by string argument to :class:`gate`'''
    
    url = 'https://en.wikipedia.org/wiki/Quantum_logic_gate'
    '''wikipedia reference'''

    def __new__(cls,*args,**kwargs):
        if debug: print('gate.__new__')
        if isinstance(args[0],str) and args[0] not in operator.ops:
            name = args[0]
            if name in ['Rx','Ry','Rz']:
                #assert 'phi' in kwargs
                assert len(args) == 2,f'{name} must include a phase argument'
                #phi = kwargs['phi']
                phi = args[1]
                if name == 'Rx':
                    self = cos(phi/2)*I - i*sin(phi/2)*X
                elif name == 'Ry':
                    self = cos(phi/2)*I - i*sin(phi/2)*Y
                elif name == 'Rz':
                    self = cos(phi/2)*I - i*sin(phi/2)*Z
                self.phi = phi
                self._name = f'Rotation_{name[-1]} ({name}) gate (@symphi = @numphi radians)'
                return self
            elif name in ['XX','YY','ZZ','XY','YZ','ZX','YX','ZY','XZ']:
                #assert 'phi' in kwargs
                assert len(args) == 2,f'{name} must include a phase argument'
                #phi = kwargs['phi']
                phi = args[1]
                if name == 'XX':
                    self = cos(phi/2)*(I**I) - i*sin(phi/2)*(X**X)
                elif name == 'YY':
                    self = cos(phi/2)*(I**I) - i*sin(phi/2)*(Y**Y)
                elif name == 'ZZ':
                    self = cos(phi/2)*(I**I) - i*sin(phi/2)*(Z**Z)
                elif name in ['XY','YX']:
                    self = exp(-i*phi/4*(X**X+Y**Y))
                elif name in ['YZ','ZY']:
                    self = exp(-i*phi/4*(Y**Y+Z**Z))
                elif name in ['ZX','XZ']:
                    self = exp(-i*phi/4*(Z**Z+X**X))
                self.phi = phi = qfloat(phi)
                s = '' if 0<abs(phi)<=1 else 's'
                self._name = f'Ising {name} gate (@symphi = @numphi radian{s})'
                return self
            elif name == 'S':
                self = gate('P',pi/2)
                self._name = name+' gate'
                return self
            elif name == 'Sjk':
                j,k,n = args[1],args[2],args[3]
                self = gate('I',n)
                self._name = f"Shor's j,k gate, {n} particles"
                w_kj = exp(i*pi/(2**(k-j)))
                d = 'd'
                b = create_basis(self.n_particles,base_repr='ud')
                q = [(l,x) for l,x in enumerate(b) if x[k+1]==d and x[j+1]==d]
                for l,x in q: self.op[l,l] = w_kj
                return self
            elif name == 'T':
                self = gate('P',pi/4)
                self._name = name+' gate'
                return self
            elif name.startswith('c'):
                name = name[1:]
                args = tuple([name]+list(args)[1:])
                self = gate(*args,**kwargs)
                return self.controlled()
            elif name.startswith('s'):
                name = name[1:]
                args = tuple([name]+list(args)[1:])
                self = gate(*args,**kwargs)
                return self.sqrt()
        self = object.__new__(cls)
        return self
    
    def __init__(self,*args, **kwargs):
        if debug: print('gate.__init__')
        if isinstance(args[0],operator):
            self.op = args[0].op
            name = ''
            if hasattr(args[0],'name'):
                name = args[0].name
            self._name = '%s gate'%name
            self.n_particles = args[0].n_particles
            self.meta = kwargs
            if 'name' in kwargs: self._name = kwargs['name']
            return
        if not isinstance(args[0],str): # matrix or list
            super(self.__class__,self).__init__(args[0])
            name = 'gate'
            if hasattr(self,'_name'):
                name = self._name
            name = kwargs.get('name',name)
            self._name = name
            self.meta = kwargs
            return
        
        name = args[0]
        name_ok = name in self.gates or name.startswith('c') or name.startswith('s')
        assert name_ok,'%s not a valid gate name'%name
        
        if name in operator.ops:
            super(self.__class__,self).__init__(name)
            self._name = '%s gate'%name
            self.meta = kwargs
            return
        
        c1 = name in ['H','X','Y','Z'] and len(args)>1
        c2 = name in ['P'] and len(args)>2
        if c1 or c2: # single qbit gate repeated on several qbits in a register
            if c1:
                j,n = args[1],args[2]
                G = gate(name)
            if c2:
                a,j,n = args[1],args[2],args[3]
                G = gate(name,a)
            if isinstance(j,list):
                if c1: Q = np.prod([gate(name,jj,n) for jj in j])
                if c2: Q = np.prod([gate(name,a,jj,n) for jj in j])
            else:
                Q = I(j)^G if j>0 else G
                Q = Q^I(n-j-1) if j<n-1 else Q
            self.op = Q.op
            self._name = f'{name}, particle {j}'  
        elif name in ['H','Hadamard']:
            self.op = (sx+sz).N.matrix
            self._name = 'Hadamard (H) gate'
        elif name in ['X','NOT']:
            self.op = sx.matrix
            self._name = 'NOT (X) gate'
        elif name == 'Y':
            self.op = sy.matrix
            self._name = 'Y gate'
        elif name == 'Z':
            self.op = sz.matrix
            self._name = 'Z gate'
        elif name == 'I':
            if len(args) > 1:
                n = args[1]
                assert isinstance(n,int),'Identity gate argument must be an integer: number of particles'
            else:
                n = 1
            self.op = np.matrix(np.identity(2**n,dtype=complex)) # I**I**...**I  n times
            self._name = f'{n} particle Identity (I) gate'
        elif name == 'P':
            assert len(args) == 2,f'{name} must include a phase argument'
            phi = args[1]
            self.op = matrix(diag([1,np.exp(i*phi)]))
            self.phi = phi
            self._name = 'Phase (P) gate (@symphi = @numphi radians)'
        elif name == 'SWAP':
            if len(args) > 1:
                j,k,n = args[1],args[2],args[3]
                G = gate('I',n)
                u,d = '0','1'
                basis01 = create_basis(G.n_particles,base_repr='01')
                al = [l for l,x in enumerate(basis01) if (x[j+1],x[k+1]) == (u,d)]
                bl = [l for l,x in enumerate(basis01) if (x[j+1],x[k+1]) == (d,u)]
                for a,b in zip(al,bl):
                    G.op[a,b] = G.op[b,a] = 1
                    G.op[a,a] = G.op[b,b] = 0
                self.op = G.op
                self._name = f'SWAP particles {j},{k}'
            else:
                self.op = matrix([[1,0,0,0],
                                  [0,0,1,0],
                                  [0,1,0,0],
                                  [0,0,0,1]])
                self._name = 'SWAP gate'
        elif name == 'iSWAP':
            self.op = matrix([[1,0,0,0],
                              [0,0,i,0],
                              [0,i,0,0],
                              [0,0,0,1]])
            self._name = 'iSWAP gate'
        n,m = self.op.shape
        self.n_particles = int(np.log2(n))
        self.basis = create_basis(self.n_particles)
        self.meta = kwargs
        if 'name' in kwargs: self._name = kwargs['name']
    
    def __repr__(self):
        s = f'{self.name}\n'
        return s + super(self.__class__,self).__repr__()

    def __mul__(self,another):
        r = super().__mul__(another)
        if isinstance(another,gate): r.name = f'{self.name} | {another.name}'
        elif isinstance(another,(int,float,complex)): r.name = f'{self.name} | {another}'
        return r
    
    def __rmul__(self,another):
        r = super().__rmul__(another)
        if isinstance(another,(int,float,complex)): r.name = f'{mprepr(another)} | {self.name}'
        return r
    
    def __pow__(self,another):
        r = super().__pow__(another)
        if np.isscalar(another):
            r.name = self.name + f'^{another}'
        return r
            
    def kron(self,another):
        r = super().kron(another)
        if isinstance(another,gate): r.name = f'{self.name} ^ {another.name}'
        return r
    
    @property
    def name(self):
        name = self._name
        if '@' in name:
            symphi = uphi if printoptions.sqrtpi else 'phi'
            numphi = prepr(self.phi) if printoptions.sqrtpi else frepr(self.phi)
            name = name.replace('@symphi',symphi).replace('@numphi',numphi)
        return name
    
    @name.setter
    def name(self,new_name):
        self._name = new_name
    
    def named(self,name):
        r = self.copy()
        r.name = name
        return r
    
    def __call__(self,*args,**kwargs):
        '''create a new gate that is this gate Kroneker'd n times
        Example: X(3) = X^X^X
        '''
        if len(args) == 0:
            r = self.copy()
            for k,v in kwargs.items():
                setattr(r,k,v)
            return r
        n = args[0]
        if isinstance(n,str):
            return self.named(n)
        assert isinstance(n,int)
        assert n>=0
        if n==0: return None
        G = self
        for x in range(n-1): G = G^self
        if self.n_particles == 1:
            name = self.name.replace('1 particle ','')
            G._name = f'{n} particle {name}'
        if self.n_particles > 1: G._name = f'{n} x replicated {self.name}'
        return G
        
    def copy(self):
        """make a copy of the gate
        """
        new = gate(self.op.copy())
        for x in ['phi','_name','control_bit']:
            if hasattr(self,x): setattr(new,x,getattr(self,x))
        return new
    
    def sqrt(self):
        '''create a square root gate from a gate.
        '''
        s = sqrtm(self.matrix)
        r = gate(s)
        r._name = f'square root {self._name}'
        if hasattr(self,'phi'): r.phi = self.phi
        return r
    
    def controlled(self):
        '''convert a gate to a controlled gate
        (introduces one more qbit as the control bit)
        '''
        n = self.op.shape[0]
        c = np.eye(n)
        r = gate(matrix(block_diag(c,self.op)))
        r._name = f'controlled {self._name}'
        if hasattr(self,'phi'): r.phi = self.phi
        return r
 
    def exp(self,*args):
        '''Exponential of the gate: e.g. exp(X,pi) == $e^{i\phi {X}}$.
        (see https://en.wikipedia.org/wiki/Quantum_logic_gate#Exponents_of_quantum_gates)
        '''
        if len(args)==0: # called like (i*phi*H).exp() or exp(i*phi*H)
            A = gate(expm(self.op))
        else: # called like H.exp(phi)
            phi = args[0]
            if self.is_involutory(): # faster calculation
                n = self.op.shape[0]
                I = gate(np.identity(n))
                A = cos(phi)*I + i*sin(phi)*self
            else:
                A = gate(expm(i*phi*self.op))
        A._name = f'exp({self._name})'
        if hasattr(self,'phi'): r.phi = self.phi
        return A
    
    def log(self):
        return gate(logm(self.op),name=f'log({self._name})')
    
    def is_involutory(self,sign=1):
        '''Test if the gate is involutory (Hermitian-Unitary), i.e. A*A = I.
        With sign = -1, check if (i*A)*(i*A) = i*I
        '''
        n = self.op.shape[0]
        return np.isclose(self.op*self.op,sign*np.identity(n)).all()

def controlled(g):
    '''g is a gate. Return a controlled g gate.
    '''
    return g.controlled()

if 'sphinx' not in sys.modules:
    sx = operator('sx')
    sy = operator('sy')
    sz = operator('sz')
    s0 = operator('s0')
    H = gate('H')
    X = gate('X')
    Y = gate('Y')
    Z = gate('Z')
    I = gate('I')
    NOT = gate('NOT')
    sNOT = gate('sNOT')
    cNOT = gate('cNOT')
    cX = cNOT
    SWAP = gate('SWAP')
    sSWAP = gate('sSWAP')
    cSWAP = gate('cSWAP')
    ccNOT = gate('ccNOT')

#=========== Measurement operator ==========
class M:
    def __init__(self,j):
        self.j = j
        self.name = f'Measure Qbit {j}'
        self.c = [self]
    def __mul__(self,a):
        if isinstance(a,state) and a.isket:
            n = a.n_particles
            s = a
            for G in self.c[::-1]:
                if G == self:
                    j = self.j
                    v,s = (I(j)^Z^I(n-j-1)).measure(s)
                elif isinstance(G,(gate,M)):
                    s = G | s
            return s
        elif isinstance(a,(gate,M)):
            self.c.append(a)
            return self
        else:
            return NotImplemented
    def __rmul__(self,a):
        j = self.j
        if isinstance(a,state) and a.isbra:
            n = a.n_particles
            v,r = (I(j)^Z^I(n-j-1)).measure(ket(a))
            return bra(r)
        elif isinstance(a,(gate,M)):
            self.c.insert(0,a)
            return self
        else:
            return NotImplemented
    def __repr__(self):
        if len(self.c) == 1 and self.c[0] == self:
            return self.name
        else:
            s = '['
            s += ' \n'.join([str(x) if x != self else self.name for x in self.c])
            s += ']'
            return s

#=========== qbit register convenience operations ===========
    
def reverse(n):
    '''a gate that reverses the order of Qbits
    
    argument
        n - the number of Qbits
    '''
    N = 2**n
    b = create_basis(n,base_repr='01')
    rb = [f"|{x.strip('|>')[::-1]}>" for x in b]
    M = np.zeros((N,N))
    for j in range(N): M[j,rb.index(b[j])]=1
    G = gate(M,name='bit reversal')
    return G

def shift(ns,n=2):
    '''an n-Gate that shifts (barrel rotate) the n Qbits to the right by ns bits
    (ns can be negative for a left shift)
    
    arguments
        ns - the integer shift amount (can be negative)
        n - the number of Qbits
    '''
    N = 2**n
    b = create_basis(n,base_repr='01')
    def sshift(s,n):
        n%=len(s)
        return s[-n:]+s[:-n]
    bs = [f"|{sshift(x.strip('|>'),ns)}>" for x in b]
    M = np.zeros((N,N))
    for j in range(N): M[j,bs.index(b[j])]=1
    G = gate(M,name=f'bit shift right by {ns} bit')
    return G

def control(G,*args,n=None):
    '''controlled gate within an n Qbit register
    
    usage:
        control(G,j,k,[n=n]) - control bit k with bit j
        control(G,k,[n=n]) - control bit k with bit 0 (j=0 is implicit)
    
    G must be a 1-Qbit gate
    j and k must be positive integers less than n and cannot be the same.
    n defaults to the max of j and k, plus 1. (bit numbering is 0,1,...,n-1)
    
    ------- <- bit 0
    ---.--- <- controlling bit (bit j)
    ---|---
    ---|---
    ---G--- <- controlled bit (bit k)
    -------
    ------- <- last bit (bit n-1)
    '''
    if len(args) == 1:
        j,k = 0,args[0]
    elif len(args) == 2:
        j,k = args
    assert k != j
    assert G.n_particles == 1
    if k > j:
        Q = G.controlled()
    elif j > k:
        j,k = k,j
        S = gate('SWAP')
        Q = S | G.controlled() | S
    if n is None: n = max(j,k)+1
    assert n>max(j,k)
    S0 = gate('SWAP',0,j,n)
    S1 = gate('SWAP',1,k,n)
    R = S0 | S1 | Q^I(n-2) | S1 | S0
    R.name = f'controlled {j}->{k} {G.name}'
    return R

def swap(j,k,n=0):
    '''an n Qbit gate that swaps Qbits j and k.
    optional argument n is the total number of Qbits, otherwise this is max(j,k)+1
    '''
    if n==0: n = max(j,k)+1
    return gate('SWAP',j,k,n)

#============ Quantum Circuits =============
class Circuit(list):
    
    def __init__(self,x=[],name='(unnamed)'):
        super().__init__(x)
        self.name = name
        
    def __repr__(self):
        s0 = f'{self._name}\n' if hasattr(self,'_name') else ''
        s = ''
        for g in self:
            s += ' '+g.__repr__().replace('\n','\n ')+'\n'
        s = '[' + s.strip() + ']'
        return s0+s

    def append(self,a,name=None):
        if isinstance(a,(gate,M)):
            a = a.copy()
            if name is not None: a.name = name
            super().append(a)
        else:
            raise NotImplementedError
    
    def __add__(self,a):
        s = Circuit(list(self) + a)
        s.name = self.name
        if isinstance(a,Circuit):
            s.name += f' + {a.name}'
        return s
    
    def __radd__(self,a):
        s = Circuit(a + list(self))
        s.name = self.name
        if isinstance(a,Circuit):
            s.name = f'{a.name} + {s.name}'
        return s        
            
    def __mul__(self,a):
        if isinstance(a,(gate,M)):
            self.append(a)
        elif isinstance(a,state) and a.kind == 'ket':
            s = a
            for G in self[::-1]:
                s = G | s
            return s
        else:
            return NotImplemented
    
    def __rmul__(self,a):
        if isinstance(a,(gate,M)):
            self.insert(0,a)
        elif isinstance(a,state) and a.kind == 'bra':
            s = a
            for G in self:
                s = s | G
            return s
        else:
            return NotImplemented
    
    def __or__(self,other):
        return self.__mul__(other)
    
    def __ror__(self,other):
        return self.__rmul__(other)
    
    def copy(self):
        return Circuit( super().copy() )
        
    def gate(self):
        '''Build a gate from the circuit
        '''
        R = None
        for G in self:
            if R is None: R = G
            else: R = R | G
        return R
    
    @property
    def names(self):
        return [x.name for x in self]
    
    @property
    def name(self):
        return self._name if hasattr(self,'_name') else None
    
    @name.setter
    def name(self,name):
        self._name = name

def full_adder(n=1):
    '''create a Qbit full adder
    
    Argument:    
        n is the number of digits in the adder
    
    '''
    if n != 1: raise NotImplementedError('only a one-digit adder is presently supported')
    cnot = gate('cNOT')
    T = gate('ccNOT')
    swap = gate('SWAP')
    S = I**I**swap
    A = cnot**I**I * I**cnot**I * I**T * cnot**I**I * S * T**I * S
    A.name = 'Full Adder circuit'
    return A

def shor():
    '''algorithm for finding prime factors using quantum computer
    '''
    raise NotImplementedError('Shor')

def qft(n,return_circuit=False):
    '''Quantum Fourier transform circuit for n Qbits
    
    arguments:
        n: the number of Qbits
    
    keyword argument:
        return_circuit: if True, returns the Circuit,
                        if False, returns the gate
    '''
    cR = {}
    for k in range(2,n+1):
        cR[k] = control(gate('P',2*pi/2**k),k-1,0)
    c = Circuit()
    for k in range(n-1):
        c.append( I(k)^H^I(n-k-1) )
        for j in range(2,n-k+1):
            c.append( I(k)^cR[j]^I(n-k-j) )
    c = c + [I(n-1)^H, reverse(n)]
    c.name = f'{n} Qbit Fourier Transform'
    if return_circuit:
        return c
    return c.gate()

def qteleport():
    raise NotImplementedError('qteleport')

def qnoclone():
    raise NotImplementedError('qnoclone')

def belltest():
    raise NotImplementedError('belltest')

def quantum_key():
    '''quantum cryptography
    '''
    raise NotImplementedError('quantum_key')
    
#====================== Tests =======================
# https://stackoverflow.com/questions/5067604/determine-function-name-from-within-that-function-without-using-traceback
# for current func name, specify 0 or no argument.
# for name of caller of current func, specify 1.
# for name of caller of caller of current func, specify 2. etc.
'''
reload(qspin.qspin)
from qspin.qspin import bra,ket,state,operator,gate,sx,sy,sz,X,Y,Z,I,set_base_repr
set_base_repr('arrow')
u = ket([1,0])
d = ket([0,1])
u+10*d

reload(qspin.qspin)
from qspin.qspin import bra,ket,state,operator,gate,sx,sy,sz,X,Y,Z,I,set_base_repr
set_base_repr('01')
u = ket('0')
d = ket('1')
s = u**d**u**u
s[1]
s[[1,4,3]]
s[1:4]
s[1:4:2]

import qspin.qspin
reload(qspin.qspin)
from qspin.qspin import bra,ket,state,operator,gate
from qspin.qspin import Cab,entropy,density,trace,ptrace,i,pi
from qspin.qspin import qfloat,qcomplex,np
from qspin.qspin import crepr,frepr,srrepr,mprepr,prepr
from qspin.qspin import exp,sqrt,abs,abs2,angle
from qspin.qspin import set_base_repr,set_printoptions
from qspin.qspin import reverse, shift, control, swap
from qspin.qspin import Circuit,full_adder,shor,qft,qteleport,qnoclone,belltest,quantum_key
set_base_repr('01')
sx,sy,sz = [operator(x) for x in 'sx,sy,sz'.split(',')]
X,Y,Z,I = [gate(x) for x in 'X,Y,Z,I'.split(',')]
u = ket('0')
d = ket('1')
H = gate('H')
print(H)
ZZ = gate('ZZ',1)
print(ZZ)
ZZ = gate('ZZ',pi)
print(ZZ)
print(X+Y)
print(Y+Z)
cX = gate('cX')
cZ = gate('cZ')
print((I^H)*cX*(I^H))
print(cZ)

cnot = gate('cNOT')
T = gate('ccNOT')
swap = gate('SWAP')
S = I**I**swap
A = cnot**I**I * I**cnot**I * I**T * cnot**I**I * S * T**I * S
A.name = 'Full Adder gate'
s = ket('1000')
sa = A*s
sa[[1,2]]
s = (u**u**u + u**u**d + u**d**u + u**d**d).N


class foo:
    def __getitem__(self,*args):
        print(len(args))
        print(list(*args)[0])
        print(list(*args))
        k = args
        print('start:{k.start},stop:{k.stop},step:{k.step}')
import qspin
from qspin import bra,ket,u,d,s0,sx,sy,sz
'''

def currentFuncName(n=0):
    return sys._getframe(n + 1).f_code.co_name

def head():
    bling = '-'*60
    print ('<%s> %s'%(currentFuncName(1),bling))

def tail():
    blin2 = '-'*30
    print ('<%s> %s PASSED %s'%(currentFuncName(1),blin2,blin2))
    
def test1():
    head()
    uuu = ket('|uuu>')
    print ('%r has %d particles'%(uuu,uuu.n_particles))
    print (uuu)
    tail()
    
def test2():
    head()
    a = ket('|ud>') + ket('|du>')
    print (a)
    b = a.H
    print (b)
    a.normalize()
    print (a)
    print (a.H)
    tail()

def test3():
    head()
    c = bra('<uuduud|')
    print ('*'*60)
    print (c.basis)
    print ('*'*60)
    tail()

def test4():
    global u,d,A,r,l
    head()
    u = state([1,0])
    d = state([0,1])
    A = u*u.H - d*d.H
    r = (u+d).normalized()
    l = (u-d).normalized()
    A*r
    print (A*r,l)
    assert A*r == l
    tail()

def test5():
    head()
    u = state('|u>')
    d = state('|d>')
    sz = u*u.H - d*d.H
    s0 = u*u.H + d*d.H
    sz1 = sz**s0
    sz2 = s0**sz
    s = u**(u+d).normalized()
    sz1.measure(s)
    sz2.measure(s)
    
    uu = u**u
    ud = u**d
    du = d**u
    dd = d**d
    s = (uu+ud).normalized()
    sz1.measure(s) # (+1,uu+ud)
    sz2.measure(s) # { (+1,uu) or (-1,ud) }
    s = (du+dd).normalized()
    sz1.measure(s) # (-1,du+dd)
    sz2.measure(s) # { (+1,uu) or (-1,dd) }
    
    s = uu + dd
    s.normalize()
    (sz**s0).measure(s) # { (+1,uu} or {-1,dd})}
    s = (u+d)**(u+d)
    s.normalize()
    (sz**s0).measure(s) # { (+1,uu+ud ) or (-1,du+dd) }

    tail()

def test6():
    head()
    u = ket([1,0])
    d = ket([0,1])
    s = (u**d - d**u).N
    Sz = u*u.H - d*d.H
    S0 = u*u.H + d*d.H
    s = ket(list(np.random.normal(size=(8)))).N
    c = s.correlation()
    print(c)
    P = [s.prob(Sz**S0**S0,ket(x)) for x in s.basis]
    print(np.sum(P))
    s1 = (u**d - 0*d**u).N**(u**d + d**u).N
    s2 = (0*u**d - d**u).N**(u**d + d**u).N
    rho = 0.5*s1.density() + 0.5*s2.density()
    print(rho)
    tail()
    
def all_tests():
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
    test_spin()
    test_entangled()
    test_ptrace()
    test_readme()

if True:
    s0 = operator('s0')
    sx = operator('sx')
    sy = operator('sy')
    sz = operator('sz')
    u = state('|u>')
    d = state('|d>')
    
    uu = u**u
    ud = u**d
    du = d**u
    dd = d**d
    # singlet state
    s = (ud - du).N
    # triplet states
    t1 = uu
    t2 = (ud + du).N
    t3 = dd
    
    # spin of an electron
    sx_e = sx/2.
    sy_e = sy/2.
    sz_e = sz/2.
    spinx = (sx_e**s0) + (s0**sx_e)
    spiny = (sy_e**s0) + (s0**sy_e)
    spinz = (sz_e**s0) + (s0**sz_e)
    spinx2 = spinx*spinx
    spiny2 = spiny*spiny
    spinz2 = spinz*spinz
    spin2 = spinx2 + spiny2 + spinz2

def test_spin():
    """ calculate the spin of an electron and pairs of electrons
    """
    head()
    print ('spin of the electron along z axis is 1/2:')
    print (u.H*sz_e*u)
    print ('magnitude of the spin is sqrt(3)/2:')
    print ('  quantum number is J=1/2 so')
    print ('  spin = sqrt( J*(J+1) ) = sqrt( (1/2)*(3/2) ) = 0.866')
    print (np.sqrt( u.H*(sx_e*sx_e + sy_e*sy_e + sz_e*sz_e)*u ))
    print ('magnitude of spin of the singlet state is zero:')
    print (np.sqrt( s.H*spin2*s ))
    print ('magnitudes of spins of the triplet states are sqrt(2):')
    print ('  quantum number of paired triplet state is J=1 so')
    print ('  spin = sqrt( J*(J+1) ) = sqrt(2) = 1.414')
    print (np.sqrt( t1.H*spin2*t1 ))
    print (np.sqrt( t2.H*spin2*t2 ))
    print (np.sqrt( t3.H*spin2*t3 ))
    print ('spins of triplet states projected onto z axis:')
    print ('  these are the m quantum nummbers and should be +1, 0, -1')
    print (t1.H*spinz*t1)
    print (t2.H*spinz*t2)
    print (t3.H*spinz*t3)
    tail()
    
def test_entangled(s = s):
    """ the argument can be any mixed state of two particles.
    the singlet state is the default argument.
    """
    head()
    print ('(perhaps) entangled state:')
    print (s)
    print ('Alice and Bob:')
    print (s.density())
    print ("Alice's view:")
    rhoA = ptrace(s.density(),[1])
    rhoB = ptrace(s.density(),[0])
    #rhoA,rhoB = s.density1()
    print (rhoA)
    print ("Bob's view:")
    print (rhoB)
    print ("entropy of Alices's view")
    S = entropy(rhoA)
    S_frac = entropy(rhoA,frac=True)
    print ('S = %.3f which is %.1f%% of max entropy'%(S,S_frac*100))
    print ("entropy of Bob's view")
    S = entropy(rhoB)
    S_frac = entropy(rhoB,frac=True)
    print ('S = %.3f which is %.1f%% of max entropy'%(S,S_frac*100))
    print ('tests for entanglement')
    rho = s.density()
    rhoA = s.density1(0)
    print ('density matrix trace test:')
    print ('trace(rho_Alice^2) =',np.trace(rhoA*rhoA))
    print ('(if it is < 1, the particles are entangled)')
    print ('correlation tests <AB> - <A><B>')
    cor = []
    scor = []
    for a,sa in zip([sx,sy,sz],['x','y','z']):
        cor_row = []
        scor_row = []
        for b,sb in zip([sx,sy,sz],['x','y','z']):
            scor_row.append('c'+sa+sb)
            sigma = (a**s0) # measures first particle without affecting 2nd
            tau = (s0**b) # measures second particle without affecting first
            c = s.H*(sigma*tau)*s - (s.H*sigma*s)*(s.H*tau*s)
            cor_row.append(c)
        scor.append(scor_row)
        cor.append(cor_row)
    cor = np.matrix(cor).real
    scor = np.matrix(scor)
    print ('correlation ')
    print (scor)
    print (cor)
    #         
    # cxx = s.H*((sx**s0)*(s0**sx))*s - (s.H*((sx**s0))*s)*(s.H*((s0**sx))*s)
    # cyy = s.H*((sy**s0)*(s0**sy))*s - (s.H*((sy**s0))*s)*(s.H*((s0**sy))*s)
    # czz = s.H*((sz**s0)*(s0**sz))*s - (s.H*((sz**s0))*s)*(s.H*((s0**sz))*s)
    # print 'correlation x,y,z = ',cxx,cyy,czz
    print ('(if any correlation != 0, the particles are entangled)')
    tail()

def test_ptrace():
    head()
    s = u**d - d**u
    s = (s**s).N
    den = ptrace(s.density(),[1,3])
    den = ptrace(s.density(),[])
    den = ptrace(s.density(),range(s.n_particles))
    tail()

def test_readme():
    head()
    # from the README.rst
    print('Spin states')
    #from qspin2 import bra,ket,u,d,s0,sx,sy,sz
    u = ket('|u>')
    d = ket('|d>')
    print(u)
    print(d)
    print(u+d)
    print(u+i*d)
    print('Operators')
    print(sx)
    print(sy)
    print(sz)
    print(sz*u)
    print(sz*d)
    print(u.H*sz*u)
    print(u)
    print(u.phi)
    print('eigenvalues and eigenvectors')
    #import numpy as np
    print(sz)
    ev,evec = np.linalg.eig(sz.matrix)
    print(ev)
    print(evec)
    print(sx) # spin x
    ev, evec = np.linalg.eig(sx.matrix)
    print(ev)
    print(evec)
    ev, evec = sx.eig()
    print(ev)
    print(evec)
    print('conditional probabilities')
    l = (u+d).N
    print(np.abs(bra(l)*ket(u))**2)
    print(np.abs(l.H*u)**2)
    print(l.prob(sz,u))
    print('string representation of state')
    set_base_repr('01')
    u = ket('0')
    d = ket('1')
    s = (u**d - d**u).N
    print(s)
    set_base_repr('arrow')
    u = ket(u'\u2193')
    d = ket(u'\u2191')
    s = (u**d-d**u).N
    print(s)
    set_base_repr('ud')
    u = ket('u')
    d = ket('d')
    
    print('partial trace')
    s = (u**d - d**u).N
    rho = s.density()
    rhoA = ptrace(rho,[1])
    print(rhoA)
    
    print('entanglement')
    #from qspin2 import ket
    u = ket('u')
    d = ket('d')
    s = (u**d - d**u).N
    print(s.entangled())
    tail()